class IDBStorage {
  constructor() {
    this.STORAGE_NAME = 'TBFStorage';
    this.STORAGE_REVISION = 1;
  }

  connect() {
    return new Promise((resolve, reject) => {
      this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
      this.db = this.indexedDB.open(this.STORAGE_NAME, this.STORAGE_REVISION);

      this.db.onupgradeneeded = () => {
        this.db.result.createObjectStore(
          this.STORAGE_NAME,
          {
            keyPath: 'key',
            autoIncrement: true
          }
        );
      };

      this.db.onsuccess = () => {
        this.transaction = this.db.result.transaction(this.STORAGE_NAME, "readwrite");
        resolve();
      };

      this.db.onerror = (error) => {
        this.db = null;
        reject('Can\'t connect to Database');
      };
    });
  }

  read() {
    return new Promise((resolve, reject) => {
      let store = this.transaction.objectStore(this.STORAGE_NAME);
      let query = store.get(1);
      query.onsuccess = () => {
        resolve(query.result && query.result.data ? query.result.data : {});
      };
      query.onerror = (error) => {
        reject(error);
      };
    });
  }

  write(data) {
    return new Promise((resolve, reject) => {
      let store = this.transaction.objectStore(this.STORAGE_NAME);
      let action = store.put({key: 1, data: data});
      action.onsuccess = () => {
        resolve();
      };
      action.onerror = (error) => {
        reject(error);
      }
    });
  }
}


export default ((function() {
  let idb_storage = new IDBStorage();

  return {
    read(callback) {
      return idb_storage
        .connect()
        .then(() => {
          return idb_storage
            .read()
            .then((result) => {
              return callback(result);
            });
        });
    },
    write(data) {
      idb_storage
        .connect()
        .then(() => {
          idb_storage.write(data)
        });
    }
  }
})());