var path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'TBFStorage.js',
    path: path.resolve(__dirname, 'dist')
  }
};