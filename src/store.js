import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';
import IDBStorage from './IDB_storage';

export default (callback) =>
  IDBStorage.read((savedState) => {
    const store = createStore(
      reducers,
      savedState,
      compose(
        applyMiddleware(thunk),
        window.devToolsExtension ? window.devToolsExtension() : f => f
      )
    );

    store.subscribe(() => {
      IDBStorage.write(store.getState());
    });

    callback(store);
  });
