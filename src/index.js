import CreateStore from './store';

window.TBF = Object.assign(window.TBF || {}, {
  Storage: CreateStore
});