const SET_USER = 'SET_USER';

export default function counter( state = null, action ) {
  switch (action.type) {
    case SET_USER:
      return action.id;
    default:
      return state;
  }
}
